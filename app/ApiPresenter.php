<?php
/**
 * GSC Tesseract
 *
 * @category Framework
 * @author   Fred Brooker <git@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 */

namespace GSC;

use Cake\Cache\Cache;
use Ehann\RediSearch\Index;
use Ehann\RedisRaw\RedisClientAdapter;
use League\Csv\Reader;
use League\Csv\Statement;
use RedisClient\RedisClient;
use Symfony\Component\Lock\Factory;
use Symfony\Component\Lock\Store\FlockStore;

/**
 * API Presenter
 */
class ApiPresenter extends APresenter
{
    /**
 * @var int maximum of records 
*/
    const MAX_RECORDS = 50;

    /**
 * @var int maximum access hits 
*/
    const MAX_API_HITS = 1000;

    /**
 * @var string API cache profile 
*/
    const API_CACHE = "hour";

    /**
 * @var bool use cache? 
*/
    const USE_CACHE = true;

    /**
 * @var int minimum CSV filesize 
*/
    const CSV_MIN_SIZE = 42;

    /**
 * @var string CSV header 
*/
    const CSV_SEZNAM_HEADER = "IDMAT,MATURAD,OKRES,OBCE,TYPMAT,ROKOD,ROKDO,OPSANYCH,CASTOPSANYCH,ZKONTROL,URL";

    /**
 * @var string CSV header - duplicates test 
*/
    const CSV_SEZNAM_HEADER_CASE_TEST = "MATURAD,OKRES,OBCE,URL";

    /**
     * @var bool force SEZNAM CSV reload in destructor 
     */
    private $force_seznam_reload = false;

    /**
 * @var string API access time limit 
*/
    const ACCESS_TIME_LIMIT = 3599;

    /**
     * Destructor
     */
    public function __destruct()
    {
        if (\ob_get_level()) {
            @\ob_flush();
        }
        \flush();
        if ($this->force_seznam_reload === true) { // rebuild CSV cache?
            $this->readCsv("seznam", true);
        }
    }

    /**
     * Main controller
     */
    public function process($param = null)
    {
        setlocale(LC_ALL, "cs_CZ.utf8");

        // check API keys
        $err = 0;
        if (isset($_GET["api"])) {
            $api = (string) $_GET["api"];
            $key = $this->getCfg("ci_tester.api_key") ?? null;
            // check CI tester key
            if ($key !== $api) {
                // invalid API key
                $err++;
            }
            // @TODO!!! check user generated key
        } else {
            // no API key
            $err++;
        }
        if ($err) {
            $this->checkRateLimit();
        }

        $view = $this->getView();
        $match = $this->getMatch();
        $extras = [
            "api_usage" => $this->accessLimiter(),
            "fn" => $view,
            "name" => "Matriky API",
            "uuid" => $this->getUID(),
        ];

        switch ($view) {
        case "GetSeznamSearch": // TBD: add caching!
            $records = $this->getSeznamSearch();
            if (is_null($records)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $data = [
                "records" => $records,
            ];
            return $this->writeJsonData($data, $extras, JSON_FORCE_OBJECT);
                break;

        case "GetSeznamYears":
            if ($cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $years = $this->getSeznamYears();
            if (is_null($years)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $data = [
                "min" => $years["min"],
                "max" => $years["max"],
                "start" => $years["od"],
                "stop" => $years["do"],
            ];
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetSeznamCount":
            if ($cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $count = $this->getSeznamCount();
            if (is_null($count)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $data = [
                "count" => $count,
            ];
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetSeznamVersion":
            if ($cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $version = $this->getSeznamVersion();
            if (is_null($version)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $data = [
                "version" => $version,
            ];
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetSeznamRecords": // TBD: add caching!
            $list = $match["params"]["list"] ?? null;
            if (is_null($list)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            $records = $this->getSeznamRecords($list);
            if (is_null($records)) {
                sleep(2);
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $count = count($records);
            $data = [
                "count" => $count,
                "records" => $records,
            ];
            return $this->writeJsonData($data, $extras, JSON_FORCE_OBJECT);
                break;

        case "GetSeznamErrors":
            if ($cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $records = $this->getSeznamErrors();
            if (is_null($records)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $data = $records;
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras, JSON_FORCE_OBJECT);
                break;

        case "GetSeznamUradyAllRecords":
            if ($cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $records = $this->getSeznamUradyAllRecords();
            if (is_null($records)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $count = count($records);
            $data = [
                "count" => $count,
                "records" => $records,
            ];
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetSeznamOkresyAllRecords":
            if ($cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $records = $this->getSeznamOkresyAllRecords();
            if (is_null($records)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $count = count($records);
            $data = [
                "count" => $count,
                "records" => $records,
            ];
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        case "GetSeznamObceAllRecords":
            if ($cache = Cache::read($view, self::API_CACHE)) {
                return $this->writeJsonData($cache, $extras);
            }
            $records = $this->getSeznamObceAllRecords();
            if (is_null($records)) {
                sleep(2); // fail
                return $this->writeJsonData(404, $extras);
            }
            // OK
            $count = count($records);
            $data = [
                "count" => $count,
                "records" => $records,
            ];
            Cache::write($view, $data, self::API_CACHE);
            return $this->writeJsonData($data, $extras);
                break;

        default:
            sleep(2); // fail
            return ErrorPresenter::getInstance()->process(404);
        }
        return $this;
    }

    private function seznamSearch()
    {
        // https://github.com/cheprasov/php-redis-client
        // http://www.ethanhann.com/redisearch-php/

        // port 6378 = RediSearch
        // port 6377 = Redis
        $redis = (new RedisClientAdapter())->connect('127.0.0.1', 6378);
        /**
                $index = new Index($redis);
                $index->drop();
                $index
                    ->addTextField('IDMAT')
                    ->addTextField('MATURAD')
                    ->addTextField('OKRES')
                    ->addTextField('OBCE')
                    ->addTextField('TYPMAT')
                    ->addNumericField('ROKOD')
                    ->addNumericField('ROKDO')
                    ->addNumericField('OPSANYCH')
                    ->addNumericField('CASTOPSANYCH')
                    ->addNumericField('ZKONTROL')
                    ->addTextField('URL')
                    ->create();
                dump($index);
                exit;
                $result = $index->search('two cities');
                return [
                    "data" => $result,
                ];
         */
    }

    /**
     * Get SEZNAM search
     *
     * @return array ID records matching the search criteria
     */
    private function getSeznamSearch()
    {
        $result = $this->seznamSearch();
        return $result;
    }

    /**
     * Get SEZNAM years
     *
     * @return array min/max/od/do integers
     */
    private function getSeznamYears()
    {
        $csv = $this->readCsv("seznam");
        if (is_array($csv) && count($csv)) {
            $od = [];
            $do = [];
            $min = 0;
            $max = 0;
            if (array_key_exists("ROKOD", $csv)) {
                $od = $csv["ROKOD"];
                $od = array_map("trim", $od);
                $od = array_filter($od, "is_numeric");
                sort($od, SORT_NUMERIC);
                $od = array_unique($od);
                $od = array_values($od);
            }
            if (array_key_exists("ROKDO", $csv)) {
                $do = $csv["ROKDO"];
                $do = array_map("trim", $do);
                $do = array_filter($do, "is_numeric");
                sort($do, SORT_NUMERIC);
                $do = array_unique($do);
                $do = array_values($do);
            }
            $x = array_merge($od, $do);
            $x = array_unique($x);
            sort($x, SORT_NUMERIC);
            $min = $x[0];
            rsort($x, SORT_NUMERIC);
            $max = $x[0];
            // OK
            return [
                "min" => $min,
                "max" => $max,
                "od" => $od,
                "do" => $do,
            ];
        }
        // fail
        return null;
    }

    /**
     * Get SEZNAM version
     *
     * @return string SHA256 hash of the array
     */
    private function getSeznamVersion()
    {
        $csv = $this->readCsv("seznam");
        if (is_array($csv) && count($csv)) {
            // OK
            return hash("sha256", json_encode($csv));
        }
        // fail
        return null;
    }

    /**
     * Get SEZNAM OBCE all records
     *
     * @return mixed array of records / null
     */
    private function getSeznamObceAllRecords()
    {
        $csv = $this->readCsv("seznam");
        if (is_array($csv) && array_key_exists("OBCE", $csv)) {
            $obce = $csv["OBCE"];
            $obce = array_map("trim", $obce);
            $obce = array_filter($obce, "strlen");
            sort($obce, SORT_LOCALE_STRING);
            $obce = array_unique($obce, SORT_LOCALE_STRING);
            $obce = array_values($obce);
            return $obce;
        }
        return null;
    }

    /**
     * Get SEZNAM MATURAD all records
     *
     * @return mixed array of records / null
     */
    private function getSeznamUradyAllRecords()
    {
        $csv = $this->readCsv("seznam");
        if (is_array($csv) && array_key_exists("MATURAD", $csv)) {
            $urady = $csv["MATURAD"];
            $urady = array_map("trim", $urady);
            $urady = array_filter($urady, "strlen");
            sort($urady, SORT_LOCALE_STRING);
            $urady = array_unique($urady, SORT_LOCALE_STRING);
            $urady = array_values($urady);
            return $urady;
        }
        return null;
    }

    /**
     * Get SEZNAM OKRESY all records
     *
     * @return mixed array of records / null
     */
    private function getSeznamOkresyAllRecords()
    {
        $csv = $this->readCsv("seznam");
        if (is_array($csv) && array_key_exists("OKRES", $csv)) {
            $okresy = $csv["OKRES"];
            $okresy = array_map("trim", $okresy);
            $okresy = array_filter($okresy, "strlen");
            sort($okresy, SORT_LOCALE_STRING);
            $okresy = array_unique($okresy, SORT_LOCALE_STRING);
            $okresy = array_values($okresy);
            return $okresy;
        }
        return null;
    }

    /**
     * Get SEZNAM count
     *
     * @return int records count
     */
    private function getSeznamCount()
    {
        $csv = $this->readCsv("seznam");
        if (is_array($csv) && array_key_exists("IDMAT", $csv)) {
            return count($csv["IDMAT"]);
        }
        return null;
    }

    /**
     * Get SEZNAM records
     *
     * @param  string $list list of records, separated by comma
     * @return array records
     */
    private function getSeznamRecords($list)
    {
        if (!strlen($list)) {
            return null;
        }
        $list = explode(",", (string) $list);
        $list = array_map("trim", $list);
        $list = array_map("intval", $list);
        $list = array_map("abs", $list);
        $list = array_filter($list, "is_int");
        $list = array_unique($list);
        if (!count($list)) {
            return null;
        }
        $columns = explode(",", self::CSV_SEZNAM_HEADER);
        $csv = $this->readCsv("seznam");
        if (is_null($csv)) {
            return null;
        }
        if (is_array($csv) && !count($csv)) {
            return null;
        }
        $count = 0;
        $records = [];
        foreach ($list as $id) {
            $arr = [];
            foreach ($columns as $k => $v) {
                if (array_key_exists($id, $csv[$v])) {
                    if ($v == "IDMAT") {
                        $arr[$v] = strtoupper($csv[$v][$id]);
                    } else {
                        $arr[$v] = $csv[$v][$id];
                    }
                } else {
                    $arr = null;
                }
            }
            $records[$id] = $arr;
            $count++;
            if ($count >= self::MAX_RECORDS) {
                // no more records!
                break;
            }
        }
        // OK
        return $records;
    }

    /**
     * Get SEZNAM errors
     *
     * @return array records
     */
    private function getSeznamErrors()
    {
        $columns = explode(",", self::CSV_SEZNAM_HEADER);
        $csv = $this->readCsv("seznam");
        if (is_null($csv) || !count($csv)) {
            return null;
        }
        $records = [];
        $records["ROKOD_ERRORS"] = [];
        $records["ROKDO_ERRORS"] = [];
        $records["IDMAT_ERRORS"] = [];
        // find ROKOD invalid years
        if (array_key_exists("ROKOD", $csv)) {
            foreach ($csv["ROKOD"] as $k => $x) {
                if (strlen($x) && !(int) $x) {
                    $records["ROKOD_ERRORS"][$k] = $x;
                }
            }
        }
        // find ROKDO invalid years
        if (array_key_exists("ROKDO", $csv)) {
            foreach ($csv["ROKDO"] as $k => $x) {
                if (strlen($x) && !(int) $x) {
                    $records["ROKDO_ERRORS"][$k] = $x;
                }
            }
        }
        // find IDMAT duplicates
        if (array_key_exists("IDMAT", $csv)) {
            $x = [];
            foreach ($csv["IDMAT"] as $k => $v) {
                if (!isset($x[$v])) {
                    $x[$v] = 1;
                } else {
                    $x[$v]++;
                }
            }
            foreach ($csv["IDMAT"] as $k => $v) {
                if (($x[$v]) >= 2) {
                    $records["IDMAT_ERRORS"][$v] = $x[$v];
                }
            }
        }
        // find case errors
        $tabs = explode(",", self::CSV_SEZNAM_HEADER_CASE_TEST);
        foreach ($tabs as $t) {
            $records["CASE_ERRORS_$t"] = [];
            if (array_key_exists($t, $csv)) {
                $keys = [];
                foreach ($csv[$t] as $k => $v) {
                    if (!strlen($v)) {
                        continue;
                    }
                    $key = mb_strtolower(json_decode('"' . str_replace('"', '\\"', $v ?? "") . '"') ?? "");
                    if (!array_key_exists($key, $keys)) {
                        $keys[$key] = [];
                    }
                    $h = hash("sha1", $v);
                    if (!array_key_exists($h, $keys[$key])) {
                        $keys[$key][$h] = 0;
                    }
                    $keys[$key][$h]++;
                }
            }
            foreach ($keys as $k => $v) {
                if (!$k) {
                    continue;
                }

                if (count($v) > 1) {
                    array_push($records["CASE_ERRORS_$t"], [$k => count($v) . " variants"]);
                }
            }
        }
        return $records;
    }

    /**
     * Read CSV file into array
     *
     * @param  string $id    CSV filename w/o extension
     * @param  mixed  $force reload? (optional)
     * @return array CSV data or empty array
     */
    private function readCsv($id, $force = null)
    {
        if (empty($id)) {
            return [];
        }
        $this->preloadAppData();
        $csv = $this->readAppData((string) $id);
        if ($csv) {
            if (strlen($csv) < self::CSV_MIN_SIZE) {
                return [];
            }
        }
        // primary / secondary cache
        $cache_key = "{$id}_csv_processed";
        $cache_key_bak = "{$id}_csv_backup";
        if (is_null($force)) {
            // primary cache
            if ($data = Cache::read($cache_key, "csv")) {
                return $data;
            }
            // secondary cache = force destructor loader
            $this->force_seznam_reload = true;
            if ($data = Cache::read($cache_key_bak, "csv_bak")) {
                return $data;
            }
            // bummer, we have no data! it will take some time...
        }
        // create Symfony file lock
        $store = new FlockStore();
        $factory = new Factory($store);
        $lock = $factory->createLock("read-csv-seznam");
        if (!$lock->acquire()) {
            // another thread is rebuilding the data
            return null;
        }
        $data = [];
        $columns = explode(",", self::CSV_SEZNAM_HEADER);
        if ($csv) {
            foreach ($columns as $col) {
                $$col = [];
                try {
                    $reader = Reader::createFromString($csv);
                    $reader->setHeaderOffset(0);
                    $records = (new Statement())->offset(1)->process($reader);
                    $i = 0;
                    foreach ($records->fetchColumn($col) as $c) {
                        $$col[$i] = $c;
                        $i++;
                    }
                    $data[$col] = $$col;
                } catch (\Exception $e) {
                    if (!LOCALHOST) {
                        $this->addError("EXCEPTION: CSV reader, column: $col");
                        $this->addError($e->getMessage());
                    }
                }
            }
            Cache::write($cache_key, $data, "csv");
            Cache::write($cache_key_bak, $data, "csv_bak");
        }
        $lock->release();
        return $data;
    }

    /**
     * Access limiter
     *
     * @return int access count
     */
    private function accessLimiter()
    {
        $hour = date("H");
        $uid = $this->getUID();
        $key = "access_limiter_" . SERVER . "_" . PROJECT . "_{$hour}_{$uid}";
        $redis = new RedisClient(
            [
            'server' => 'localhost:6377',
            'timeout' => 1,
            ]
        );
        try {
            $val = (int) @$redis->get($key);
        } catch (\Exception $e) {
            return null;
        }
        if ($val > self::MAX_API_HITS) { // over the limit!            
            $this->setLocation("/err/420");
        }
        try {
            @$redis->multi();
            @$redis->incr($key);
            @$redis->expire($key, self::ACCESS_TIME_LIMIT);
            @$redis->exec();
        } catch (\Exception $e) {
            return null;
        }
        $val++;
        return $val;
    }
}
