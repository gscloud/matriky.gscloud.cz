<?php // @author Filip Oščádal <oscadal@gscloud.cz>

# constants
define("ROOT", __DIR__."/..");
define("CACHE", ROOT."/cache");

ob_start();
session_start();
error_reporting(E_ALL);
ini_set("auto_detect_line_endings", "1");
ini_set("display_errors", "1");

# classes
require_once ROOT."/vendor/autoload.php";

# configuration
use \Nette\Neon\Neon;
define("CONFIG", ROOT."/config.neon");
$cfg = Neon::decode(file_get_contents(CONFIG));
date_default_timezone_set($cfg["date_default_timezone"] ?? "Europe/Prague");

# debugger
use Tracy\Debugger;
if (!isset($cfg["dbg"])) $cfg["dbg"] = FALSE;
Debugger::enable( ((bool) $cfg["dbg"]) ? Debugger::DEVELOPMENT : Debugger::PRODUCTION);
Debugger::$strictMode = TRUE;
Debugger::$maxDepth = 50;
Debugger::$maxLength = 5000;
Debugger::timer();

# csv settings
$range = "A1:Z";
$spreadsheetId = $cfg["google_sheet"] ?? 0;

# fake data
$x = [];
for ($i=0; $i < 10; $i++) array_push($x, rand(1000, 10000));
$values = [$x];

# Google API client
$client = new \Google_Client();
$client->setApplicationName($cfg["applicationname"]);
$client->useApplicationDefaultCredentials();
$client->setScopes([\Google_Service_Sheets::SPREADSHEETS]);
$client->setAuthConfig(ROOT."/".$cfg["google_credentials"] ?? "");
$client->setAccessType("offline");

# Sheets service
$service = new \Google_Service_Sheets($client);
$body = new \Google_Service_Sheets_ValueRange(["values" => $values]);
$params = ["valueInputOption" => $cfg["google_sheet_valueinputoption"]];
$result = $service->spreadsheets_values->append($spreadsheetId, $range, $body, $params);

echo "Saved.";
