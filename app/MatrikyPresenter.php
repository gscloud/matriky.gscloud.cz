<?php
/**
 * GSC Tesseract
 *
 * @category Framework
 * @author   Fred Brooker <oscadal@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 */

namespace GSC;

use Cake\Cache\Cache;
use There4\Analytics\AnalyticsEvent;

/** 
 * Matriky Presenter
 */
class MatrikyPresenter extends APresenter
{

    /**
     * Main controller
     *
     * @return object Singleton instance
     */
    public function process()
    {
        $this->checkRateLimit()->setHeaderHtml();

        $data = $this->getData();
        $presenter = $this->getPresenter();
        $view = $this->getView();

        // expand data model
        $this->dataExpander($data);

        // form switch
        $data["container_switch_form"] = true;

        // fix all text
        $language = $data["lang"];
        foreach ($data["l"] as $k => $v) {
            StringFilters::correctTextSpacing($v, $language);
            $data["l"][$k] = $v;
        }

        // render output & save to model & cache
        $output = $this->setData($data)->renderHTML($presenter[$view]["template"]);
        StringFilters::trimHtmlComment($output);
        return $this->setData("output", $output);
    }
}
