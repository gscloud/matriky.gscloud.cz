<?php
/**
 * GSC Tesseract
 *
 * @category Framework
 * @author   Fred Brooker <oscadal@gscloud.cz>
 * @license  MIT https://gscloud.cz/LICENSE
 * @link     https://lasagna.gscloud.cz
 */

namespace GSC;

class FormPresenter extends APresenter
{

    public function process()
    {
        $data = $this->getData();
        $presenter = $this->getPresenter();
        $view = $this->getView();

        $this->checkRateLimit();

        // check user
        $data["user"] = $this->getCurrentUser();
        $data["admin"] = $this->getUserGroup();
        if ($this->getUserGroup()) {
            $data["admin_group_" . $this->getUserGroup()] = true;
        }

        // get user data
#        $data["admin"] = $this->getAdminGroup();
#        $data["user"] = [];
#        $data["user"]["id"] = $this->getCookie("id") ?? "";
#        $data["user"]["name"] = $this->getCookie("name") ?? "";
#        $data["user"]["email"] = $this->getCookie("admin") ?? "";
#        $data["user"]["avatar"] = $this->getCookie("avatar") ?? "";

        // set language and fetch locale
        $language = strtolower($presenter[$view]["language"]) ?? "cs";
        $data["lang"] = $language;
        $data["lang{$language}"] = true;
        $data["l"] = $this->getLocale($language);
        $data["DATA_VERSION"] = hash('sha256', (string) json_encode($data["l"]));

        // fix all text
        foreach ($data["l"] as $k => $v) {
            $v = StringFilters::correctTextSpacing($v, $language);
            $v = StringFilters::convertEolToBr($v);
            $data["l"][$k] = $v;
        }

        // OUTPUT
        $output = $this->setData($data)->renderHTML($presenter[$view]["template"]);
        $output = StringFilters::trimHtmlComment($output);
        $data["output"] = $output;
        $this->setData($data);
        return $this;
    }

}
